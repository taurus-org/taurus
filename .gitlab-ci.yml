stages:
  - check
  - build
  - test
  - deploy

run-pre-commit:
  stage: check
  image: registry.gitlab.com/tango-controls/docker/pre-commit
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  cache:
    key: pre-commit-cache
    paths:
      - ${PRE_COMMIT_HOME}
  script:
    - pre-commit run --all-files

build-pypi-package:
  stage: build
  image: python:3.9-slim-bullseye
  needs: []
  before_script:
    - pip install --upgrade pip
    - pip install --upgrade build
  script:
    - python -m build
  artifacts:
    expire_in: 1 day
    paths:
      - dist/

.default_rules:
  rules:
    # Disable job if TAURUS_TEST_IMAGE is passed
    - if: $TAURUS_TEST_IMAGE
      when: never
    # Disable detached pipeline on MR
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
    - when: always

doc:
    # build docs
    extends: .default_rules
    stage: build
    image: registry.gitlab.com/taurus-org/taurus-docker:conda-3.9-9.3.6
    needs: []
    variables:
        PUBLIC_URL: /-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public
    before_script:
        - pip install sphinx sphinx-rtd-theme sphinxcontrib-mermaid
    script:
        - xvfb-run sphinx-build -qW doc/source/ public
    artifacts:
        paths:
            - public
    environment:
        name: Docs-dev
        url: "https://$CI_PROJECT_NAMESPACE.gitlab.io/-/$CI_PROJECT_NAME/-/jobs/$CI_JOB_ID/artifacts/public/index.html"


.run_tests:
    stage: test
    needs: ["build-pypi-package"]
    variables:
        GIT_STRATEGY: none
        PYTEST_QT_API: $QT_API
    image:
        name: registry.gitlab.com/taurus-org/taurus-docker:$TAURUS_TAG
    before_script:
        # On bookworm, installing a package with pip requires a virtualenv by default
        # due to the implementation of https://peps.python.org/pep-0668
        # Need to use --break-system-packages to force installation
        - python3 -m pip install dist/taurus-*.whl || python3 -m pip install --break-system-packages dist/taurus-*.whl
    script:
        - INST_DIR=$(python3 -c 'import taurus; print(taurus.__file__.rsplit("/",1)[0])')
        - python3 -m pytest --junitxml=report.xml -n3 --forked -v $INST_DIR
    artifacts:
        when: always
        reports:
            junit: report.xml

tests:
    extends: [.run_tests, .default_rules]
    # run test suite with various py versions
    parallel:
        matrix:
            - TAURUS_TAG: ["bookworm", "bullseye", "buster"]
              # disable bullseye pyside2 tests until they are green
              # QT_API: ["pyqt5", "pyside2"]
              QT_API: ["pyqt5"]
            - TAURUS_TAG: ["conda-3.9-9.3.6", "conda-3.10-9.3.6", "conda-3.11-9.3.6"]
              QT_API: ["pyqt5", "pyside2"]
            - TAURUS_TAG: ["conda-3.6", "conda-3.7-9.3.6", "conda-3.8-9.3.6", "conda-3.12-9.5.1", "conda-3.12-10.0.0"]
              QT_API: ["pyqt5"]

# Run tests with a specific image
# TAURUS_TEST_IMAGE variable can be passed from an upstream project
# or by triggering a pipeline manually
run-test-specific-image:
    extends: .run_tests
    image: ${TAURUS_TEST_IMAGE}
    variables:
        QT_API: ${TAURUS_TEST_QT_API}
    rules:
        - if: $TAURUS_TEST_IMAGE && $TAURUS_TEST_QT_API


pages:
    # deploy docs
    stage: deploy
    image: alpine
    variables:
        GIT_STRATEGY: none
    script:
        - echo "Deploying already-built docs"
    artifacts:
        paths:
            - public
    rules:
        - if: '$CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+.*$/'


deploy_to_pypi:
    stage: deploy
    image: python:3.9-slim-bullseye
    variables:
        GIT_STRATEGY: none
    before_script:
        - pip install twine
    script:
        - twine upload dist/*
    rules:
        - if: '$CI_COMMIT_TAG =~ /^[0-9]+\.[0-9]+\.[0-9]+.*$/'
